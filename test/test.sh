
#!/bin/bash

set -x
set -e

##script to test the succesful execution of the wrapper. Expected output: "merged_freqs.csv: OK"
cp "$(pwd)"/test/merged.bam "$(pwd)"/
sh "$(pwd)"/bamtofreq.sh



echo "845a3e4e8e1a96d165f18b283445ebc9  merged_freqs.csv" | md5sum -c -


#md5sum -c <<<"845a3e4e8e1a96d165f18b283445ebc9 merged_freqs.csv"
