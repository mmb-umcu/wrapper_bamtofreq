## Purpose

Converts a bam file to a frequency table. This frequency table can be used as the input for [geno2pheno](https://hcv.geno2pheno.org)

Wrapper script around bamToFreq, using the [bamtoFreq](https://github.com/matdoering/bamToFreq) docker container.

## Usage

Details and wiki entry can be found at the [internal wiki](http://143.121.18.159/doku.php?id=file_conversion:bam_to_frequency_tables)

Will be executed as

```
./bamtofreq.sh
``` 
and expects an input file 'merged.bam'

This input file is an output of the genexus sequencer and always has the name 'merged.bam' 


## Requirements 

Requires docker and a shell (bash or sh)


17-3-2022 - 29-3-2022 Anita Schurch

### Changelog
20220329 first release with working CI

