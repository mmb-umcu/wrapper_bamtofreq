#!/bin/bash
set -e
set -x

#ASchurch 17032022
#executed as ./bamtofreq.sh
#requires an input file with the name merged.bam


#check if input folder, otherwise generate this directory
mkdir -p input

#initiate log file
timestamp="$(date +"%T")"
mkdir -p "$(pwd)"/log
log=$(pwd)/log/bamtofreq_"$timestamp".txt
touch "$log"
sleep 1


#remove old output
if test -f "merged_freqs.csv" ;
then 
rm -f merged_freqs.csv 
fi


#check if input is there and move to input directory
if test -f "merged.bam" ;
then
echo "merged.bam is found"  | tee -a "$log"
cp merged.bam $(pwd)/input/
else
echo "merged.bam is not found, please copy merged.bam to this location" | tee -a "$log"
exit 
fi


#run the docker container
docker run --network=host -v $(pwd)/input/:/data/ mdoering88/bamtofreq:latest /data/merged.bam  | tee -a "$log"

#singularity pull docker://mdoering88/bamtofreq:latest
#singularity exec -H $PWD -B $PWD/input/:/data/  docker://mdoering88/bamtofreq:latest /data/merged.bam  | tee -a "$log"

#mv output one up
if test -f "input/merged_freqs.csv" ;
then 
mv input/merged_freqs.csv $(pwd)/ 
rm -f input/merged_codonFreqs.csv
else
echo "output was not produced. Exiting."  | tee -a "$log"
exit 
fi

#rm input from input and roor and output from root for next time

if test -f "merged.bam" ;
then
echo "merged.bam is being deleted"  | tee -a "$log"
rm  merged.bam
fi


#check if correct output is there
if test -f "merged_freqs.csv" ;
then 
echo "converted file is called merged_freqs.csv"  | tee -a "$log"
fi

